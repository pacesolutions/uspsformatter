package pacesolutions.uspsformatter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextToSpeech tts;
    EditText trackingNumber;
    ToggleButton numberInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        trackingNumber = (findViewById(R.id.trackingNumber));
        (findViewById(R.id.formatButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResult();
            }
        });

        (findViewById(R.id.speakResult)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speakResult(trackingNumber.getText().toString());
            }
        });

        numberInput = (findViewById(R.id.numberInput));
        numberInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateToggleButtonAction();
            }
        });

        //make the keyboard show
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                trackingNumber.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                trackingNumber.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);

        updateToggleButtonAction();
    }

    private void updateToggleButtonAction(){
        if(numberInput.isChecked()){
            trackingNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
            numberInput.setText("Numbers Only");
        }else{
            trackingNumber.setInputType(InputType.TYPE_CLASS_TEXT);
            numberInput.setText("Allow Text");
        }
    }

    private void showResult(){
        trackingNumber.setText(formatInput(trackingNumber.getText().toString().trim()));
        ((ClipboardManager) getSystemService(CLIPBOARD_SERVICE)).setPrimaryClip(ClipData.newPlainText("", trackingNumber.getText()));
        Toast.makeText(MainActivity.this, "Copied", Toast.LENGTH_SHORT).show();
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(trackingNumber.getWindowToken(), 0);
    }

    public static String formatInput(String words){
        String numbersInbetweenSpaces = "4";
        String inputString = words.replace(" ", "");
        inputString = inputString.replaceAll("[^a-zA-Z0-9]", "");
        return inputString.replaceAll("(.{" + numbersInbetweenSpaces + "})", "$0 ").trim();
    }

    private void speakResult(final String result){
        tts = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.setLanguage(Locale.US);
                tts.speak(result, TextToSpeech.QUEUE_FLUSH, null, null);
            }
        });
    }
}
