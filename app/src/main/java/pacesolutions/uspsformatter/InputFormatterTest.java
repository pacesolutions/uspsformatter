package pacesolutions.uspsformatter;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class InputFormatterTest {

    public final String expectedOutput = "1234 abcd 5678 efgh 9";

    @Test
    public void numberFormatValidate_SimpleFormat() {
        assertEquals(MainActivity.formatInput("1234abcd5678efgh9"), expectedOutput);
    }

    @Test
    public void numberFormatValidate_ContainsBadCharsAndSpaces() {
        assertEquals(MainActivity.formatInput("  1 !2@3#4$:a%;b^'c&<d,*>5.(?/6)7 8 e f g h 9       "), expectedOutput);
    }
}